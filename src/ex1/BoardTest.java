package ex1;

import org.junit.Test;
import runner.Points;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class BoardTest {

    @Test
    @Points(3)
    public void squaresCanBeSet() {

        Board board = new Board(5, 6); // 5 rows, 6 columns

        // XOOOOO
        // OOOOOO
        // OOXOOO
        // OOOOOO
        // OOOOXO
        board.set(1, 1);
        board.set(3, 3);
        board.set(5, 5);

        assertThat(board.isSet(1, 1), is(true));
        assertThat(board.isSet(3, 3), is(true));
        assertThat(board.isSet(5, 5), is(true));

        assertThat(board.isSet(1, 2), is(false));
        assertThat(board.isSet(4, 4), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    @Points(4)
    public void settingOutsideTheBoardThrows() {
        new Board(0, 0).set(1, 1);
    }

    @Test
    @Points(5)
    public void boardKnowsIfSquareIsIsolated() {

        Board board = new Board(5, 6);

        assertThat(board.isIsolated(3, 3), is(true));

        // OOOOOO
        // OOOOOO
        // OOXOOO
        // OOOOOO
        // OOOOOO
        board.set(3, 3);

        assertThat(board.isIsolated(2, 2), is(false));
        assertThat(board.isIsolated(2, 3), is(false));
        assertThat(board.isIsolated(2, 4), is(false));

        assertThat(board.isIsolated(3, 2), is(false));
        assertThat(board.isIsolated(3, 3), is(false));
        assertThat(board.isIsolated(3, 4), is(false));

        assertThat(board.isIsolated(4, 2), is(false));
        assertThat(board.isIsolated(4, 3), is(false));
        assertThat(board.isIsolated(4, 4), is(false));

        assertThat(board.isIsolated(4, 5), is(true));
    }

    @Test
    @Points(3)
    public void canHandleEdges() {

        Board board = new Board(3, 4);

        // XOOO
        // OOOO
        // OOOX
        board.set(1, 1);
        board.set(3, 4);

        assertThat(board.isIsolated(1, 1), is(false));
        assertThat(board.isIsolated(1, 2), is(false));
        assertThat(board.isIsolated(3, 3), is(false));
        assertThat(board.isIsolated(3, 4), is(false));

        assertThat(board.isIsolated(1, 4), is(true));
    }
}
