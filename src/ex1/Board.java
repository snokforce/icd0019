package ex1;

import java.util.List;

public class Board {

    private boolean[][] board;

    public Board(int rows, int columns) {
        this.board = new boolean[rows][columns];
    }

    public void set(int row, int column) {
        try {
            this.board[row - 1][column - 1] = true;
        } catch (ArrayIndexOutOfBoundsException e){
            throw new IllegalArgumentException();
        }

    }

    public boolean isSet(int row, int column) {
        try {
            return this.board[row - 1][column - 1];
        } catch (ArrayIndexOutOfBoundsException e){
            throw new IllegalArgumentException();
        }
    }

    public boolean isIsolated(int row, int column) {

        if (isSet(row, column)) return false;

        for(int i = -1; i <= 1; i++){
            for(int j = -1; j <= 1; j++){
                try {
                    if(isSet(row + i, column + j)) return false;
                } catch (IllegalArgumentException e){
                    continue;
                }

            }
        }

        return true;
    }
}
