package fp;

import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;


public class Persons {

    private List<Person> persons = List.of(
            new Person(1, "Alice", 22),
            new Person(2, "Bob", 20),
            new Person(3, "Carol", 21));

    @Test
    public void findsPersonById() {

        List<Person> result = persons.stream()
                .filter(c -> c.getId().equals(2))
                .collect(Collectors.toList());

        System.out.println(result);
    }

    @Test
    public void removePersonById() {

        List<Person> result = persons.stream()
                .filter(c -> !c.getId().equals(2))
                .collect(Collectors.toList());

        System.out.println(result);

    }

    @Test
    public void findsPersonNames() {
        List<String> result = persons.stream()
                .map(c -> c.getName())
                .collect(Collectors.toList());

        System.out.println(result);
    }

    @Test
    public void reverseSortedByAge() {
        List<Person> result = persons.stream()
                .sorted((a, b) -> b.getAge().compareTo(a.getAge()))
                .collect(Collectors.toList());

        System.out.println(result);

    }

}
