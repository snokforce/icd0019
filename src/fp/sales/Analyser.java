package fp.sales;


import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;


public class Analyser {

    private Repository repository;


    public Analyser(Repository repository) {
        this.repository = repository;
    }

    public Double getTotalSales() {
        return repository.getRepository().stream().skip(1)
                .map(line -> line.split("\t")[5])
                .map(s -> s.replace(",", "."))
                .mapToDouble(Double::parseDouble)
                .sum();
    }

    public Double getSalesByCategory(String category) {
        return repository.getRepository().stream().skip(1)
                .map(line -> line.split("\t"))
                .filter(cat -> cat[3].equals(category))
                .map(line -> line[5])
                .map(l -> l.replace(",", "."))
                .mapToDouble(Double::parseDouble)
                .sum();
    }

    public Double getSalesBetween(LocalDate start, LocalDate end) {
        return repository.getRepository().stream().skip(1)
                .map(line -> line.split("\t"))
                .filter(line -> repository.getDate(line[0]).isAfter(start) && repository.getDate(line[0]).isBefore(end))
                .map(line -> line[5])
                .map(l -> l.replace(",", "."))
                .mapToDouble(Double::parseDouble)
                .sum();
    }

    public String mostExpensiveItems() {
        return repository.getRepository().stream().skip(1)
                .map(line -> line.split("\t"))
                .sorted(Comparator.comparing(l -> Double.parseDouble(l[5].replace(",", ".")), Comparator.reverseOrder()))
                .limit(3)
                .map(line -> line[2])
                .sorted()
                .collect(Collectors.joining(", "));
    }

    public String statesWithBiggestSales() {
        Map<String, Double> lines = repository.getRepository().stream().skip(1)
                .map(line -> line.split("\t"))
                .collect(Collectors.groupingBy(l -> l[1],
                        Collectors.mapping(l -> l[5], Collectors.summingDouble(s -> Double.parseDouble(s.replace(",", "."))))))
                ;
        return lines.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(3)
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(", "));
    }
    
}
