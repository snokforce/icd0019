package collections.streaks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Code {

    public static List<List<String>> getStreakList(String input) {
        List<List<String>> streak = new ArrayList<>();
        List<String> currentStreak = new ArrayList<>();

        for (String s : input.split("")) {
            if (currentStreak.size() == 0 || currentStreak.contains(s)) {
                currentStreak.add(s);
            }
            else {
                streak.add(currentStreak);
                currentStreak = new ArrayList<>();
                currentStreak.add(s);
            }
        }
        streak.add(currentStreak);
        return streak;

    }

}
