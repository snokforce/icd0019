package collections.simulator;

import java.util.*;

public class Hand implements Iterable<Card> {

    private List<Card> cards = new ArrayList<>();

    public void addCard(Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        return cards.toString();
    }

    public HandType getHandType() {
        Hand handValue = this;
        int placeInHand = 0;
        int[] values = new int[handValue.handSize()];
        for (Card card : handValue) {
            values[placeInHand] = cardValue(card.getValue());
            placeInHand += 1;
        }
        Arrays.sort(values);
        return checkForCombo(values, handValue);
    }

    public int cardValue(Card.CardValue cardValue) {
        switch (cardValue) {
            case S2:
                return 2;
            case S3:
                return 3;
            case S4:
                return 4;
            case S5:
                return 5;
            case S6:
                return 6;
            case S7:
                return 7;
            case S8:
                return 8;
            case S9:
                return 9;
            case S10:
                return 10;
            case J:
                return 11;
            case Q:
                return 12;
            case K:
                return 13;
            case A:
                return 14;
            default:
                return 0;
        }
    }

    public int pairCount(int[] cardsValues) {
        int pairCount = 0;
        ArrayList<Integer> pairTypes = new ArrayList<>();
        for (int element : cardsValues) {
            int counter = 0;
            for (int cardsValue : cardsValues) {
                if (element == cardsValue) {
                    counter++;
                }
            }
            if (counter > 1 && !pairTypes.contains(element)) {
                pairCount += 1;
                pairTypes.add(element);
            }
        }
        return pairCount;
    }

    public boolean tripComboCheck(int[] cardsValues) {
        if (cardsValues.length < 3) {
            return false;
        }
        if (cardsValues[0] == cardsValues[1] && cardsValues[1] == cardsValues[2]) {
            return true;
        } else if (cardsValues[1] == cardsValues[2] && cardsValues[2] == cardsValues[3]) {
            return true;
        } else if (cardsValues.length == 5) {
            return cardsValues[2] == cardsValues[3] && cardsValues[3] == cardsValues[4];
        }
        return false;
    }

    public boolean fourOfAKindComboCheck(int[] cardValues) {
        if (cardValues.length < 4) {
            return false;
        }
        if (cardValues[0] == cardValues[1] && cardValues[1] == cardValues[2] && cardValues[2] == cardValues[3]) {
            return true;
        } else {
            return cardValues[1] == cardValues[2] && cardValues[2] == cardValues[3] &&
                    cardValues[3] == cardValues[4];
        }
    }

    public boolean fullHouseComboCheck(int[] cardsValues) {
        if (cardsValues.length != 5) {
            return false;
        }
        if (cardsValues[0] == cardsValues[1] && cardsValues[1] == cardsValues[2]) {
            return cardsValues[3] == cardsValues[4];
        } else if(cardsValues[2] == cardsValues[3] && cardsValues[3] == cardsValues[4]) {
            return cardsValues[0] == cardsValues[1];
        }
        return false;
    }

    public boolean flushComboCheck(Hand handValue) {
        int[] cardSuits = new int[4];
        for (Card card : handValue) {
            if (card.getSuit() == Card.CardSuit.C){
                cardSuits[0] += 1;
            } else if (card.getSuit() == Card.CardSuit.D){
                cardSuits[1] += 1;
            } else if (card.getSuit() == Card.CardSuit.H){
                cardSuits[2] += 1;
            } else if (card.getSuit() == Card.CardSuit.S){
                cardSuits[3] += 1;
            }
        }
        for (int cardSuit : cardSuits) {
            if (cardSuit == 5) {
                return true;
            }
        }
        return false;
    }

    public boolean straightComboCheck(int[] cardValues) {
        int[] straightWithA = {2, 3, 4, 5, 14};
        if (cardValues.length == 5) {
            if (Arrays.equals(straightWithA, cardValues)) {
                return true;
            }
            return cardValues[0] + 1 == cardValues[1] && cardValues[1] + 1 == cardValues[2] &&
                    cardValues[2] + 1 == cardValues[3] && cardValues[3] + 1 == cardValues[4];
        } return false;
    }


    public HandType checkForCombo(int[] cardsValues, Hand handValue) {
        if (straightComboCheck(cardsValues)) {
            if (flushComboCheck(handValue)) {
                return HandType.STRAIGHT_FLUSH;
            } else {
                return HandType.STRAIGHT;
            }
        } else if(fourOfAKindComboCheck(cardsValues)) {
            return HandType.FOUR_OF_A_KIND;
        } else if (tripComboCheck(cardsValues)) {
            if (fullHouseComboCheck(cardsValues)) {
                return HandType.FULL_HOUSE;
            }
            return HandType.TRIPS;
        } else if (flushComboCheck(handValue)) {
            return HandType.FLUSH;
        } else {
            if (pairCount(cardsValues) > 1) {
                return HandType.TWO_PAIRS;
            } else if (pairCount(cardsValues) == 1) {
                return HandType.ONE_PAIR;
            } else {
                return HandType.HIGH_CARD;
            }
        }
    }

    public boolean contains(Card card) {
        return cards.contains(card);
    }

    public boolean isEmpty() {
        return cards.isEmpty();
    }

    @Override
    public Iterator<Card> iterator() {
        return cards.iterator();
    }

    public Integer handSize() {
        int count = 0;
        for (Card cardValue : this) {
            if (cardValue != null) {
                count ++;
            }
        }
        return count;
    }
}
