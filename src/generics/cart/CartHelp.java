package generics.cart;

import java.util.Objects;

public class CartHelp {

    private CartItem item;
    private Integer quantity;

    public CartHelp(CartItem item, Integer quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public CartItem getItem() {
        return item;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CartHelp cartEntry = (CartHelp) o;
        return Objects.equals(item, cartEntry.item);
    }

    @Override
    public int hashCode() {
        return Objects.hash(item);
    }
}
