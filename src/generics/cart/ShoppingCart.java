package generics.cart;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


public class ShoppingCart<T extends CartItem> {

    private List<CartHelp> cart = new ArrayList<>();
    Double discountSummary = 1.0;
    Double discountTheLastOne;

    public void add(T item) {
        CartHelp cartItemsAdd = new CartHelp(item, 1);
        if (cart.contains(cartItemsAdd)) {
            increaseQuantity(cartItemsAdd.getItem().getId());
        } else {
            cart.add(cartItemsAdd);
        }
    }

    public void removeById(String id) {
        for (CartHelp item : cart) {
            if (item.getItem().getId().equals(id)) {
                cart.remove(item);
                return;
            }
        }
    }

    public Double getTotal() {
        return cart.stream()
                .map(item -> item.getQuantity() * item.getItem().getPrice())
                .reduce(0.0, Double::sum) * discountSummary;

    }

    @Override
    public String toString() {
        return cart.stream()
                .map(i -> String.format(Locale.US,"(%s, %.1f, %d)", i.getItem().getId(), i.getItem().getPrice(), i.getQuantity()))
                .collect(Collectors.joining(", "));
    }

    public void increaseQuantity(String id) {
        for (CartHelp item : cart) {
            if (id.equals(item.getItem().getId())) {
                item.setQuantity(item.getQuantity() + 1);
            }
        }
    }

    public void applyDiscountPercentage(Double discount) {
        discountSummary = discountSummary * (1 - discount / 100);
        discountTheLastOne = discount;
    }

    public void removeLastDiscount() {
        discountSummary = discountSummary / (1 - discountTheLastOne / 100);
    }

    public void addAll(List<CartItem> items) {
        items.forEach(item -> this.add((T) item));
    }
}
