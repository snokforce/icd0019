package generics.cart;

import java.util.Objects;

public class Product implements CartItem {
    private String id;
    private Double price;

    public Product(String id, Double price) {
        this.id = id;
        this.price = price;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(price, product.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price);
    }
}
