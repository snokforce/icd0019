package junit;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class Tests {

    @Test
    public void equalityExamples() {
        assertTrue(1 == 1);
        assertFalse(1 == 2);

        Integer x2 = 1;
        Integer y2 = 1;
        assertTrue(x2 == y2);

        Integer x = 128;
        Integer y = 128;
        assertFalse(x == y);
        assertTrue(x.equals(y));

//   "abc" == "abc" on tõene
//   "abc" == "a" + "bc" on tõene
        assertTrue("abc" == "abc");
        assertTrue("abc" == "a" + "bc");

        //String a = "a";
        //   "abc" == a + "bc" on väär
        //   "abc".equals(a + "bc") on tõene

        String a = "a";
        assertFalse("abc" == a + "br");
        assertTrue("abc".equals(a + "bc"));
    }

    @Test
    public void assertThatAndAssertEqualsExample() {
        assertEquals(1 + 2, 3);

        assertThat(1 + 3, is(4));

        assertThat(1 + 3, is(equalTo(4)));

        assertThat(1 + 3, is(not(5)));

        //    new int[] {1, 2, 3} on new int[] {1, 2, 3}

        assertThat(new int[] {1, 2, 3}, is(new int[] {1, 2, 3}));

        assertThat(new int[] {1, 2, 3}, is(not(new int[] {1, 2})));
    }

    @Test
    public void findsSpecialNumbers() {
        assertTrue(Code.isSpecial(0));
        assertTrue(Code.isSpecial(11));
        assertTrue(Code.isSpecial(12));
        assertTrue(Code.isSpecial(2));
        assertTrue(Code.isSpecial(3));
        assertFalse(Code.isSpecial(4));
        assertTrue(Code.isSpecial(11));
        assertFalse(Code.isSpecial(15));
        assertTrue(Code.isSpecial(36));
        assertFalse(Code.isSpecial(37));

        // other test cases for isSpecial() method
    }

    @Test
    public void findsLongestStreak() {
        assertThat(Code.longestStreak("abbb"), is(3));

        // other test cases for longestStreak() method
    }

    @Test
    public void findsModeFromCharactersInString() {

        assertThat(Code.mode(null), is(nullValue()));
        assertThat(Code.mode("cbbc"), is("c"));
        assertThat(Code.mode("b"), is("b"));
        assertThat(Code.mode("abcb"), is("b"));

        // other test cases for mode() method
    }

    @Test
    public void removesDuplicates() {
        assertThat(Code.removeDuplicates(arrayOf(1, 1)), is(arrayOf(1)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 1, 2)), is(arrayOf(1, 2)));

        assertThat(Code.removeDuplicates(arrayOf(1, 2, 3)), is(arrayOf(1, 2, 3)));
    }

    @Test
    public void sumsIgnoringDuplicates() {
        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 1)), is(1));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 1, 2)), is(3));

        assertThat(Code.sumIgnoringDuplicates(arrayOf(1, 2, 3)), is(6));
    }

    private int[] arrayOf(int... numbers) {
        return numbers;
    }

}
