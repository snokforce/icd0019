package junit;


public class Code {

    public static boolean isSpecial(int number) {
        int remainder = number % 11;
        return remainder == 0 || remainder == 1 || remainder == 2 || remainder == 3;
    }

    public static int longestStreak(String input) {
        if (input.length() == 0) {
            return 0;
        }
        if (input.length() == 1) {
            return 1;
        }
        int max = 0;
        for (int i = 0; i < input.length(); i++) {
            int count = 0;
            for (int j = i; j < input.length(); j++) {
                if (input.charAt(i) == input.charAt(j)) {
                    count++;
                }
                else {
                    break;
                }
                if (count > max) {
                    max = count;
                }
            }
        }
        return  max;
    }

    public static Character mode(String input) {
        if (input == null || input.length() == 0) {
            return null;
        }
        if (input.length() == 1) {
            return input.charAt(0);
        }
        String mostUsedChar = "";
        int count = 0;

        String[] array = input.split("");

        for (String value : array) {
            int tempCount = 0;
            for (String s : array) {
                if (value.equals(s)) {
                    tempCount++;
                }
                if (tempCount > count) {
                    count = tempCount;
                    mostUsedChar = value;
                }
            }
        }
        return mostUsedChar.charAt(0);
    }

    public static int getCharacterCount(String input, char c) {
        if (input == null) {
            return 0;
        }
        if (input.length() == 0) {
            return 0;
        }
        int count = 0;

        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == c) {
                count++;
            }
        }
        return count;
    }

    public static int[] removeDuplicates(int[] input) {
        if (input.length == 0 || input.length == 1) {
            return input;
        }
        int len = input.length;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < len; i++) {

            for (int j = i + 1; j < len; j++) {
                if (input[i] == input[j]) {
                    input[j] = 0;

                }
            }
            if (input[i] != 0) {
                sb.append(input[i]);
            }
        }
        String[] test = sb.toString().split("");
        int[] res = new int[test.length];
        for (int i = 0; i < test.length; i++) {
            res[i] = Integer.parseInt(test[i]);
        }
        return res;
    }

    public static int sumIgnoringDuplicates(int[] integers) {
        int len = integers.length;
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (integers[i] == integers[j]) {
                    integers[j] = 0;
                }
            }
            if (integers[i] != 0) {
                sb.append(integers[i]);
            }
        }
        String[] test = sb.toString().split("");
        int[] res = new int[test.length];
        for (int i = 0; i < test.length; i++) {
            res[i] = Integer.parseInt(test[i]);
        }
        int sum = 0;
        for (int re : res) {
            sum += re;
        }
        return sum;
    }
}
