package oo.hide;

public class PointSet {
    int count = 0;
    Integer capacity;
    private Point[] points;


    public PointSet(int capacity) {
        this.points = new Point[capacity];
        this.capacity = capacity;
    }

    public PointSet() {
        this(10);
    }

    public void add(Point point) {
        size();
        if (capacity != count) {
            for (int i = 0; i < points.length; i++) {
                if (null == points[i]) {
                    points[i] = point;
                    break;
                }
                if (point.equals(points[i])) {
                    break;
                }
            }
        } else {
            Point[] p = new Point[capacity * 2];
            points = p;
        }
    }

    public int size() {
        int count = 0;
        for (Point point : points) {
            if (point != null) {
                count++;
            }
        }
        this.count = count;
        return count;
    }

    public boolean contains(Point point) {
        for (Point value : points) {
            if (value != null && value.equals(point)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Point point : points) {
            if (point != null) {
                result.append(point).append(", ");
            }
        }
        result = result.deleteCharAt(result.length() - 2);
        return result.toString().trim();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PointSet)) {
            return false;
        }
        PointSet pointSet = (PointSet) o;
        for (Point point : points) {
            if (!pointSet.contains(point) && point != null) {
                return false;
            }
        }
        return true;
    }

    public PointSet subtract(PointSet other) {
        PointSet result = new PointSet();
        for (Point point : points) {
            if (!other.contains(point) && point != null) {
                result.add(point);
            }
        }
        return result;
    }

    public PointSet intersect(PointSet other) {
        PointSet result = new PointSet();
        for (Point point : points) {
            if (point != null && other.contains(point)) {
                result.add(point);
            }
        }
        return result;
    }
}