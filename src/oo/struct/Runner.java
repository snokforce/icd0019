package oo.struct;

import org.junit.Test;

public class Runner {

    @Test
    public void coordinatesAsArrays() {

        Point3D[] trianglePoints = {
                new Point3D(1, 1, 0),
                new Point3D(5, 1, 0),
                new Point3D(3, 7, 1)};

        for (Point3D each : trianglePoints) {
            System.out.println(each.z);
        }

    }

    @Test
    public void coordinatesAsObjects() {

    }


}
