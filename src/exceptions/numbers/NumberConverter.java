package exceptions.numbers;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class NumberConverter {

    private Properties properties;

    public NumberConverter(String lang) {
        String filePath = null;
        switch (lang) {
            case "et":
                filePath = "src/exceptions/numbers/numbers_et.properties";
                break;
            case "fr":
                filePath = "src/exceptions/numbers/numbers_fr.properties";
                break;
            case "es":
                filePath = "src/exceptions/numbers/numbers_es.properties";
                break;
            case "cu":
                filePath = "src/exceptions/numbers/numbers_cu.properties";
                break;
            case "en":
                filePath = "src/exceptions/numbers/numbers_en.properties";
                break;
            default:
                break;
        }

        this.properties = new Properties();
        FileInputStream numAndW = null;
        try {
            numAndW = new FileInputStream(Objects.requireNonNull(filePath));

            InputStreamReader reader;
            reader = new InputStreamReader(
                    numAndW, StandardCharsets.UTF_8);

            properties.load(reader);
        } catch (NullPointerException | IOException e) {
            throw new MissingLanguageFileException(lang, e);
        } catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException(lang, e);
        }  finally {
            close(numAndW);
        }
    }

    public String numberInWord(int num, int onesNumbersInHundreds, int hundredsNumbers, int onesNumbers) {
        if (properties.containsKey(String.valueOf(num))) {
            return properties.getProperty(String.valueOf(num));
        } else {
            String hundredsInNumbers = properties.getProperty(String.valueOf(hundredsNumbers)) +
                    properties.getProperty("hundreds-before-delimiter") +
                    properties.getProperty("hundred") + properties.getProperty("hundreds-after-delimiter");
            if (properties.containsKey(onesNumbersInHundreds + String.valueOf(onesNumbers))) {
                if (onesNumbersInHundreds == 0 && onesNumbers == 0) {
                    return properties.getProperty(String.valueOf(hundredsNumbers)) +
                            properties.getProperty("hundreds-before-delimiter") +
                            properties.getProperty("hundred");
                } else {
                    return hundredsInNumbers
                            + properties.getProperty(onesNumbersInHundreds + String.valueOf(onesNumbers));
                }
            }
            if (1 == onesNumbersInHundreds) {
                if (properties.containsKey(onesNumbersInHundreds + String.valueOf(onesNumbers))) {
                    return hundredsInNumbers + properties.getProperty(onesNumbersInHundreds + String.valueOf(onesNumbers));
                } else {
                    return hundredsInNumbers + properties.getProperty(String.valueOf(onesNumbers)) + properties.getProperty("teen");
                }
            } else {
                return moreCodeNumberInWords(onesNumbersInHundreds, onesNumbers, hundredsInNumbers, hundredsNumbers);
            }
        }
    }

    public String moreCodeNumberInWords(Integer onesNumbersInHundreds, Integer onesNumbers, String hundredsInNumbers, Integer hundredsNumbers) {
        if (properties.containsKey(onesNumbersInHundreds + "0")) {
            if (0 == onesNumbers) {
                return hundredsInNumbers + properties.getProperty(onesNumbersInHundreds + "0");
            } else {
                return hundredsInNumbers + properties.getProperty(onesNumbersInHundreds + "0") +
                        properties.getProperty("tens-after-delimiter") +
                        properties.getProperty(String.valueOf(onesNumbers));
            }
        } else {
            if (0 == onesNumbers) {
                if (0 == onesNumbersInHundreds) {
                    return properties.getProperty(String.valueOf(hundredsNumbers)) +
                            properties.getProperty("hundreds-before-delimiter") + properties.getProperty("hundred");
                } else {
                    return hundredsInNumbers + properties.getProperty(String.valueOf(onesNumbersInHundreds)) +
                            properties.getProperty("tens-suffix");
                }
            } else {
                if (0 == onesNumbersInHundreds) {
                    return hundredsInNumbers + properties.getProperty(String.valueOf(onesNumbers));
                } else {
                    return hundredsInNumbers + properties.getProperty(String.valueOf(onesNumbersInHundreds)) +
                            properties.getProperty("tens-suffix") + properties.getProperty("tens-after-delimiter") +
                            properties.getProperty(String.valueOf(onesNumbers));
                }
            }
        }
    }

    public String numberInWords(Integer num) {
        exists(num);
        if (num / 100 == 0) {
            if (num / 10 == 0) {
                return properties.getProperty(String.valueOf(num));
            } else {
                if (properties.containsKey(String.valueOf(num))) {
                    return properties.getProperty(String.valueOf(num));
                }
                if (1 == num / 10) {
                    final String key = num / 10 + String.valueOf(num % 10);
                    if (properties.containsKey(key)) {
                        return properties.getProperty(key);
                    } else {
                        return properties.getProperty(String.valueOf(num % 10)) + properties.getProperty("teen");
                    }
                } else {
                    if (properties.containsKey(num / 10 + "0")) {
                        return properties.getProperty(num / 10 + "0") + properties.getProperty("tens-after-delimiter") +
                                properties.getProperty(String.valueOf(num % 10));
                    } else {
                        if (0 == num % 10) {
                            return properties.getProperty(String.valueOf(num / 10)) + properties.getProperty("tens-suffix");
                        } else {
                            return properties.getProperty(String.valueOf(num / 10)) + properties.getProperty("tens-suffix") +
                                    properties.getProperty("tens-after-delimiter") + properties.getProperty(String.valueOf(num % 10));
                        }
                    }
                }
            }
        } else {
            return numberInWord(num, (num / 10) % 10, num / 100, num % 10);
        }
    }

    public void close(FileInputStream streamInput) {
        if (streamInput == null) {
            return;
        }
        try {
            streamInput.close();
        } catch (IOException ignore) {}
    }

    public void exists(int num) {

        if (properties.getProperty(String.valueOf(num % 10)) == null) {
            throw new MissingTranslationException(String.valueOf(num % 10));
        }
        if (num / 100 == 0) {
            if (properties.getProperty(String.valueOf(num / 10)) == null) {
                throw new MissingTranslationException(String.valueOf(num / 10));
            }
        } else {
            if (properties.getProperty(String.valueOf(num / 100)) == null) {
                throw new MissingTranslationException(String.valueOf(num / 100));
            }
            if (properties.getProperty(String.valueOf((num / 10) % 10)) == null) {
                throw new MissingTranslationException(String.valueOf((num / 10) % 10));
            }
        }
    }
}