package inheritance.analyser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaxFreeSalesAnalyser extends AbstractTaxSalesAnalyser {

    public TaxFreeSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    public Double getTotalSales() {
        for (SalesRecord record : records) {
            totalsales += record.getItemsSold() * record.getProductPrice();
        }
        return totalsales;
    }

    @Override
    public Double getTotalSalesByProductId(String id) {
        for (SalesRecord record : records) {
            if (record.getProductId().equals(id)) {
                salesbyid += record.getItemsSold();
            }
        }
        return salesbyid;
    }

}
