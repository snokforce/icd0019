package inheritance.analyser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FlatTaxSalesAnalyser extends AbstractTaxSalesAnalyser {

    public FlatTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    public Double getTotalSales() {
        for (SalesRecord record : records) {
            totalsales += (record.getProductPrice() * record.getItemsSold()) / 1.2;
        }
        return totalsales;
    }

    @Override
    public Double getTotalSalesByProductId(String id) {
        for (SalesRecord record : records) {
            if (record.getProductId().equals(id)) {
                salesbyid += record.getItemsSold() / 1.2;
            }
        }
        return salesbyid;
    }

}
