package inheritance.analyser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractTaxSalesAnalyser {
    public Double totalsales = 0.0;
    public Double totalsalesreduced = 0.0;
    public Double salesbyid = 0.0;
    List<SalesRecord> records;

    public AbstractTaxSalesAnalyser(List<SalesRecord> records) {
        this.records = records;
    }

    public abstract Double getTotalSales();

    public abstract Double getTotalSalesByProductId(String id);

    public String getIdOfMostPopularItem() {
        ArrayList<String> ids = new ArrayList<>();
        for (SalesRecord record : records) {
            String unsortedid = record.getProductId();
            if (!ids.contains(unsortedid)) {
                ids.add(unsortedid);
            }
        }
        int min = 0;
        int max = 0;
        String bestid = "";
        for (String id : ids) {
            for (SalesRecord record : records) {
                if (id.equals(record.getProductId())) {
                    min = record.getItemsSold();
                    if (min > max) {
                        max = min;
                        bestid = id;
                    }
                }
            }
        }
        return bestid;
    }

    public String getIdOfItemWithLargestTotalSales() {
        ArrayList<String> ids = new ArrayList<>();
        for (SalesRecord record : records) {
            String unsortedid = record.getProductId();
            if (!ids.contains(unsortedid)) {
                ids.add(unsortedid);
            }
        }
        Collections.sort(ids);
        int min = 0;
        int max = 0;
        String largest = "";
        for (String id : ids) {
            for (SalesRecord record : records) {
                if (id.equals(record.getProductId())) {
                    min = min + record.getItemsSold();
                    if (min > max) {
                        max = min;
                        largest = id;
                    }
                }
            }
        }
        return largest;
    }
}
