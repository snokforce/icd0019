package inheritance.analyser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DifferentiatedTaxSalesAnalyser extends AbstractTaxSalesAnalyser {

    public DifferentiatedTaxSalesAnalyser(List<SalesRecord> records) {
        super(records);
    }

    @Override
    public Double getTotalSales() {
        for (SalesRecord record : records) {
            if (record.hasReducedRate()) {
                totalsalesreduced += record.getItemsSold() * record.getProductPrice() / 1.1;
            }
            else {
                totalsales += record.getItemsSold() * record.getProductPrice() / 1.2;
            }
        }
        return totalsales + totalsalesreduced;
    }

    @Override
    public Double getTotalSalesByProductId(String id) {
        for (SalesRecord record : records) {
            if (record.getProductId().equals(id) && record.hasReducedRate()) {
                salesbyid = record.getItemsSold() / 1.1;
            }
            else {
                salesbyid = record.getItemsSold() / 1.2;
            }
        }
        return salesbyid;
    }

}
