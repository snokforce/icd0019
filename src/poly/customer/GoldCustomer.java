package poly.customer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class GoldCustomer extends AbstractCustomer {

    private LocalDate lastOrderDate;

    public GoldCustomer(String id, String name,
                        int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);

        this.lastOrderDate = lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() > 100) {
            this.bonusPoints += order.getTotal() * 1.5;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GoldCustomer other = (GoldCustomer) o;
        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) && Objects.equals(bonusPoints, other.bonusPoints) &&
                Objects.equals(lastOrderDate, other.lastOrderDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints, lastOrderDate);
    }

    @Override
    public String asString() {
        return String.format("%s;%s;%s;%d;%s;", "GOLD", this.id, this.name, this.bonusPoints, this.lastOrderDate.toString());
    }

}