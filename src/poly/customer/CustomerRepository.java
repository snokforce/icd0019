package poly.customer;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CustomerRepository {

    private static final String FILE_PATH = "src/poly/customer/data.txt";
    List<String> lines;

    public Optional<AbstractCustomer> getCustomerById(String id) {
        try {
            lines = Files.readAllLines(Paths.get(FILE_PATH));
        } catch (IOException e) {
            Optional.empty();
        }

        List<String[]> customersPayload = lines.stream()
                .map(line -> line.split(";"))
                .collect(Collectors.toList());

        for (String[] strings : customersPayload) {
            if (strings[1].equals(id)) {
                if (strings[0].equals("REGULAR")) {
                    AbstractCustomer customer = new RegularCustomer(strings[1], strings[2], Integer.parseInt(strings[3]), LocalDate.parse(strings[4]));
                    return Optional.of(customer);
                } else {
                    AbstractCustomer goldCustomer = new GoldCustomer(strings[1], strings[2], Integer.parseInt(strings[3]), LocalDate.now());
                    return Optional.of(goldCustomer);
                }
            }
        }
        return Optional.empty();
    }

    public void remove(String id) {
        try {
            List<String> data = Files.readAllLines(Paths.get(FILE_PATH));
            FileWriter writer = new FileWriter(CustomerRepository.FILE_PATH, false);
            List<String> newData = new ArrayList<>();
            for (String datum : data) {
                String[] payLoad = datum.split(";");
                if (!payLoad[1].equals(id)) {
                    newData.add(String.join(";", payLoad));
                }
            }
            writer.write(String.join("\n", newData));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    public void save(AbstractCustomer customer) {
        if (getCustomerById(customer.id).isPresent()) {
            remove(customer.id);
        }
        try {
            List<String> data = Files.readAllLines(Paths.get(FILE_PATH));
            FileWriter writer = new FileWriter(CustomerRepository.FILE_PATH, false);
            List<String> newData = new ArrayList<>();
            for (String datum : data) {
                String[] payLoad = datum.split(";");
                if (!payLoad[1].equals(customer.getId())) {
                    newData.add(String.join(";", payLoad));
                }
            }
            newData.add(customer.asString());
            writer.write(String.join("\n", newData));
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    public int getCustomerCount() {
        try {
            return Files.readAllLines(Paths.get(FILE_PATH)).size();
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }
}

