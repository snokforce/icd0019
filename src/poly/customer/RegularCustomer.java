package poly.customer;

import generics.cart.Product;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Objects;

public class RegularCustomer extends AbstractCustomer {

    public LocalDate lastOrderDate;

    public RegularCustomer(String id, String name,
                           int bonusPoints, LocalDate lastOrderDate) {

        super(id, name, bonusPoints);

        this.lastOrderDate = lastOrderDate;
    }

    @Override
    public void collectBonusPointsFrom(Order order) {
        if (order.getTotal() > 100) {
            if (Period.between(lastOrderDate, order.getDate()).getMonths() < 1) {
                this.bonusPoints += order.getTotal() * 1.5;
            } else {
                this.bonusPoints += order.getTotal();
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RegularCustomer other = (RegularCustomer) o;
        return Objects.equals(id, other.id) &&
                Objects.equals(name, other.name) && Objects.equals(bonusPoints, other.bonusPoints) &&
                Objects.equals(lastOrderDate, other.lastOrderDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonusPoints, lastOrderDate);
    }

    @Override
    public String asString() {
        return String.format("%s;%s;%s;%d;%s;", "REGULAR", this.id, this.name, this.bonusPoints, this.lastOrderDate.toString());
    }

}