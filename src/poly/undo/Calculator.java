package poly.undo;

import java.util.Stack;

public class Calculator {

    private double value;

    private Stack<Double> undos = new Stack<>();

    public void input(double value) {
        undos.push(this.value);
        this.value = value;
    }

    public void add(double addend) {
        undos.push(this.value);
        value += addend;
    }

    public void multiply(double multiplier) {
        undos.push(this.value);
        value *= multiplier;
    }

    public double getResult() {
        return value;
    }

    public void undo() {
        value = undos.pop();
    }
}
