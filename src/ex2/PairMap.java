package ex2;

import java.util.HashMap;
import java.util.Map;

public class PairMap <K extends Object, V extends Object>{

    private Map<K, Pair<V>> objectPairMap;

    public PairMap(){
        objectPairMap = new HashMap<>();
    }

    public void put(K key, V first, V second) {
        objectPairMap.put(key, new Pair<V>(first, second));
    }

    public Pair<V> get(Object key) {
        return objectPairMap.get(key);
    }

    private void compareGenerics(V first, V second){

    }

}
