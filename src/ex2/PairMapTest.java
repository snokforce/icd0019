package ex2;

import org.junit.Test;
import runner.Points;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class PairMapTest {

    @Test
    @Points(5)
    public void canCreateAPairForArbitraryType() {

        Pair<Float> pair = new Pair<>(1f, 2f);

        Float first = pair.getFirst();
        Float second = pair.getSecond();

        assertThat(first, is(1f));
        assertThat(second, is(2f));
    }

    @Test
    @Points(5)
    public void pairMapStoresPairObjects() {

        PairMap<String, Integer> map = new PairMap<>();

        map.put("k1", 1, 2);

        Integer first = map.get("k1").getFirst();
        Integer second = map.get("k1").getSecond();

        assertThat(first, is(1));
        assertThat(second, is(2));
    }

    @Test
    @Points(5)
    public void pairMapStoresPairValuesInSortedOrder() {

        PairMap<Integer, String> map = new PairMap<>();

        map.put(1, "a", "b");
        map.put(2, "b", "a");
        map.put(3, "c", "b");

        assertThat(map.get(1).getFirst(), is("a"));
        assertThat(map.get(1).getSecond(), is("b"));

        assertThat(map.get(2).getFirst(), is("a"));
        assertThat(map.get(2).getSecond(), is("b"));

        assertThat(map.get(3).getFirst(), is("b"));
        assertThat(map.get(3).getSecond(), is("c"));
    }


}
