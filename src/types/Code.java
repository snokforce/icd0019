package types;

import java.util.Arrays;

public class Code {

    public static void main(String[] args) {

        int[] numbers =  {1, 2, 3};
        String squareD = "a2b";

        System.out.println(sum(numbers));
        System.out.println(average(numbers));
        System.out.println(minimumElement(numbers));
        System.out.println(asString(numbers));
        System.out.println(squareDigits(squareD));
    }

    public static int sum(int[] numbers) {
        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        return sum;
    }

    public static double average(int[] numbers) {
        double sum = 0.0;
        for (int number : numbers) {
            sum += number;
        }
        return sum / numbers.length;
    }

    public static Integer minimumElement(int[] integers) {
        if (integers.length == 0) {
            return null;
        }
        int min = integers[0];

        for (int each : integers) {
            if (each < min) {
                min = each;
            }
        }
        return min;
    }

    public static String asString(int[] elements) {
        return Arrays.toString(elements).replace("[", "").replace("]", "");
    }

    public static String squareDigits(String s) {
        char[] charArray = s.toCharArray();
        StringBuilder res = new StringBuilder();

        for (char c : charArray) {
            if (Character.isDigit(c)) {
                String str = Character.toString(c);
                Integer square = Integer.parseInt(str);
                res.append(square * square);
            }
            else {
                res.append(c);
            }
        }
     return res.toString();
    }
}